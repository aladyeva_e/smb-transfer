package edu.wustl.artyomovlab.smbtransfer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.DigestInputStream;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.util.StringJoiner;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import jcifs.CIFSContext;
import jcifs.config.PropertyConfiguration;
import jcifs.context.BaseContext;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;


public class App 
{

    private static String getHexaString(byte[] data) {
        String result = new BigInteger(1, data).toString(16);
        return result;
       }
      
       private static String getMessageDigestOut(DigestOutputStream digestOutputStream) {
        MessageDigest digest = digestOutputStream.getMessageDigest();
        byte[] digestBytes = digest.digest();
        String digestStr = getHexaString(digestBytes);
        return digestStr;
       }

       private static String getMessageDigestIn(DigestInputStream digestInputStream) {
        MessageDigest digest = digestInputStream.getMessageDigest();
        byte[] digestBytes = digest.digest();
        String digestStr = getHexaString(digestBytes);
        return digestStr;
       }

       
       
    public static void main( String[] args )
    {

        String inputFilePath = "";
        String outputFilePath = "";
        String username = "";
        String password = "";
        int blockSize = 5242880;
        Boolean removeFile = false;

        //Options definition

        final Options options = new Options();
        Option inputFile = Option.builder("i").hasArg().longOpt("input").desc("Input filepath (absolute/relative)").build();
        inputFile.setType(String.class);
        Option outputFile = Option.builder("o").hasArg().longOpt("output").desc("Filepath on SMB server (full path, including server address and smb://)").build();
        outputFile.setType(String.class);
        Option user = Option.builder("u").hasArg().longOpt("user").desc("WUSTL key").build();
        user.setType(String.class);
        Option pass = Option.builder("p").hasArg().longOpt("pass").desc("WUSTL password").build();
        user.setType(String.class);
        Option block = Option.builder("b").hasArg().longOpt("block-size").desc("Block size, default 5242880").build();
        block.setType(Integer.class);
        Option remove = Option.builder("d").longOpt("delete").desc("Delete on server file if exists (boolean), default false").build();
        remove.setType(Boolean.class);

        Option help = Option.builder("h").longOpt("help").desc("Show this message").build();
        remove.setType(Boolean.class);

        options.addOption(inputFile);
        options.addOption(outputFile);
        options.addOption(user);
        options.addOption(pass);
        options.addOption(block);
        options.addOption(remove);
        options.addOption(help);

        

        //Options parsing

        CommandLineParser parser = new DefaultParser();

        try {
        CommandLine cmd = parser.parse( options, args);
        if (cmd.hasOption("h")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "java -jar SMBTransfer-0.1.jar", options );
            return;
        }

        if (cmd.hasOption("i")) {
            inputFilePath = cmd.getOptionValue("i");
        }
        else {
            throw new ParseException("Input file wasn't specified");
        }
        if (cmd.hasOption("o")) {
            outputFilePath = cmd.getOptionValue("o");
        }
        else {
            throw new ParseException("Output file wasn't specified");
        }

        if (cmd.hasOption("u")) {
            username = cmd.getOptionValue("u");
        }
        else {
            throw new ParseException("Username wasn't specified");
        }

        if (cmd.hasOption("p")) {
            password = cmd.getOptionValue("p");
        }
        else {
            throw new ParseException("Password wasn't specified");
        }

        if (cmd.hasOption("b")) {
            blockSize = Integer.getInteger(cmd.getOptionValue("b"));
        }

        if (cmd.hasOption("d")) {
            removeFile = cmd.hasOption("d");
        }
        
        }
        catch( ParseException exp ) {
            System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
            return;
        }


        
        try {
            //SMB connection
            BaseContext bc = new BaseContext(new PropertyConfiguration(System.getProperties()));
            NtlmPasswordAuthentication creds = new NtlmPasswordAuthentication(bc, "ACCOUNTS.AD.WUSTL.EDU", username, password);
            CIFSContext ct = bc.withCredentials(creds);
            
            
            //Example
            //outputFilePath = "smb://storage1.ris.wustl.edu/martyomov/Active/IndividualBackUps/aladyevae/md5_java/test.txt"
            SmbFile remoteFile = new SmbFile(outputFilePath,ct);
            if (remoteFile.isFile() && remoteFile.exists()) {
                if (removeFile) {
                    remoteFile.delete();
                }
                else {
                    remoteFile.close();
                    System.out.println("File "+outputFilePath+" exists");
                    return;
                }
                
            }

            OutputStream out = new BufferedOutputStream(new SmbFileOutputStream( remoteFile ));
            DigestOutputStream digestOutputStream = new DigestOutputStream(out, MessageDigest.getInstance("md5")); 
            InputStream in = new BufferedInputStream(new FileInputStream( inputFilePath));
            DigestInputStream digestInputStream = new DigestInputStream(in, MessageDigest.getInstance("md5"));

            long t0 = System.currentTimeMillis();

            byte[] b = new byte[blockSize];
        int n, tot = 0;
        while(( n = digestInputStream.read( b )) > 0 ) {
            digestOutputStream.write( b, 0, n );
            tot += n;
            //System.out.print( '#' );
        }

        
        long t = System.currentTimeMillis() - t0;

        String digestAfterRead = getMessageDigestIn(digestInputStream);
        String digestAfterWrite = getMessageDigestOut(digestOutputStream);

        StringJoiner joiner = new StringJoiner("\t");
        joiner.add(inputFilePath);
        joiner.add(outputFilePath);
        joiner.add(new Integer(blockSize).toString());
        joiner.add(new Integer(tot).toString() + " bytes");
        joiner.add(new Long(( t / 1000 )).toString() + " seconds");
        joiner.add((( tot / 1000 ) / Math.max( 1, ( t / 1000 ))) + " Kbytes/sec");
        joiner.add(digestAfterRead);
        joiner.add(digestAfterWrite);

        String joinedString = joiner.toString();
        System.out.println(joinedString);

        digestInputStream.close();
        digestOutputStream.close();

        remoteFile.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
