# SMB Transfer

Tool for uploading files to SMB servers. Additionally calculates checksums of 
input and output streams.

## Usage


```
usage: java -jar SMBTransfer.jar
 -b,--block-size <arg>   Block size, default 5242880
 -d,--delete             Delete on server file if exists (boolean),
                         default false
 -h,--help               Show this message
 -i,--input <arg>        Input filepath (absolute/relative)
 -o,--output <arg>       Filepath on SMB server (full path, including
                         server address and smb://)
 -p,--pass <arg>         WUSTL password
 -u,--user <arg>         WUSTL key
```

## Output format

Tab separated output to command line
- Input filepath
- Output filepath
- Block size
- File size
- Upload time
- Upload speed
- MD5 hash of input stream
- MD5 hash of output stream
 

## Bulk loading
Example of bash file with instructions for bulk upload files from directory

```
#!/bin/bash

USERNAME="user"
PASSWORD="password"
INPUT_FOLDER="./input_folder"
# Folder should exists on the server
SERVER_FOLDER="smb://storage1.ris.wustl.edu/martyomov/Active/IndividualBackUps/aladyevae/md5_java/input_folder"

for input_file in $INPUT_FOLDER/*
do
  echo "Processing $input_file file..."
  java -jar SMBTransfer.jar -i $input_file -o $SERVER_FOLDER/$(basename $input_file) -d -u $USERNAME -p $PASSWORD
done
```
